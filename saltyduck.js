loaded(async function() {
    if (document.body.innerHTML.indexOf("Sign in") == -1) {
        await start();
    } else {
        document.getElementById("balancewrapper").textContent = "Sign in to start playing and to use Saltyduck! ";
    }
});

async function loaded(callback) {
    if (document.readyState === 'complete') {
        callback();
    } else {
        window.addEventListener('load', callback, false);
    }
}

async function start() {
    if (typeof(Storage) == "undefined") {
        alert("Sorry! Saltyduck requires HTML web storage and your browser version doesn't support it!");
        return false;
    }

    createStatElement("player1", "p1stats");
    createStatElement("player2", "p2stats");

    var observer = new MutationObserver(async function(mutations, observer) {
        await checkStatus();
    });

    var target = document.getElementById("betstatus");
    observer.observe(target, {characterData: true, childList:true, attributes: false});
    console.info("Saltyduck loaded");
}

async function checkStatus() {
    try {
        const req = await fetch("https://www.saltybet.com/state.json");
        const data = await req.json();
        checkData(data);
    } catch (e) {
        console.warn("unable to fetch data");
    }
}

function checkData(data) {
    if (data.status == localStorage.getItem('status')) {
        return;
    } else {
        localStorage.setItem('status', data.status);
    }

    if (data.status == 1) { // P1 win
        updateResults(data.p1name, data.p2name);
    } else if (data.status == 2) { // P2 win
        updateResults(data.p2name, data.p1name);
    } else if (data.status !== "locked") { // Bets open
        bet(data.p1name, data.p2name);
    } else { // Bets locked
        document.getElementById("p1stats").style.removeProperty("color");
        document.getElementById("p2stats").style.removeProperty("color");
        checkStorage();
    }
}

function updateResults(winner, loser) {
    var match = winner + ":" + loser;
    if (match === localStorage.getItem('match')) {
        return;
    } else {
        localStorage.setItem('match', match);
    }

    winnerData = getFighterData(winner);
    loserData = getFighterData(loser);

    winnerElo = winnerData[0];
    winnerAgg = winnerData[1] + loserData[0];
    winnerWins = winnerData[2] + 1;
    winnerLosses = winnerData[3];
    loserElo = loserData[0];
    loserAgg = loserData[1] + winnerData[0];
    loserWins = loserData[2];
    loserLosses = loserData[3] + 1;

    newWinnerElo = (winnerAgg + 400 * (winnerWins - winnerLosses))/(winnerWins + winnerLosses);
    newLoserElo = (loserAgg + 400 * (loserWins - loserLosses))/(loserWins + loserLosses);

    winnerData = [newWinnerElo.toFixed(0), winnerAgg, winnerWins, winnerLosses].join(":");
    loserData = [newLoserElo.toFixed(0), loserAgg, loserWins, loserLosses].join(":");

    localStorage.setItem(winner, winnerData);
    localStorage.setItem(loser, loserData);
}

function bet(p1, p2) {
    p1Data = getFighterData(p1);
    p2Data = getFighterData(p2);

    p1Card = document.getElementById("p1stats");
    p2Card = document.getElementById("p2stats");

    p1Stats = "<br><br><p>W: " + p1Data[2] + " L: " + p1Data[3] + " E: " + p1Data[0] + "</p>";
    p1Card.innerHTML = p1Stats;
    p2Stats = "<br><br><p>W: " + p2Data[2] + " L: " + p2Data[3] + " E: " + p2Data[0] + "</p>";
    p2Card.innerHTML = p2Stats;

    if (p1Data[0] > p2Data[0]) {
        p1Card.style.color = "green";
        if (p1Data[0] > p2Data[0] * 2) {
            p2Card.style.color = "grey";
        }
    } else if (p2Data[0] > p1Data[0]) {
        p2Card.style.color = "green";
        if (p2Data[0] > p1Data[0] * 2) {
            p1Card.style.color = "grey";
        }
    } else {
        p1Card.style.color = "yellow";
        p2Card.style.color = "yellow";
    }
}

function createStatElement(parent, id) {
    card = document.getElementById(parent).parentElement.parentElement;
    node = document.createElement("div");
    node.id = id;
    node.style.textAlign = "center";
    node.style.fontSize = "105%";
    card.appendChild(node);
}

function getFighterData(fighter) {
    if (!localStorage.getItem(fighter)) {
        localStorage.setItem(fighter, "1000:0:0:0");
    }
    return localStorage.getItem(fighter).split(":").map(numStr => parseInt(numStr));
}

function checkStorage() {
    var _lsTotal = 0,
        _xLen, _x;
    for (_x in localStorage) {
        if (!localStorage.hasOwnProperty(_x)) {
            continue;
        }
        _xLen = ((localStorage[_x].length + _x.length) * 2);
        _lsTotal += _xLen;
    };
    
    KBs = (_lsTotal / 1024).toFixed(0);
    if (KBs >= 3840) {
        console.warn("Local storage over 75% full");
    } else if (KBs >= 4608) {
        alert("Local storage over 90% full");
    }
}
    